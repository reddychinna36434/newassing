package week2assignment;
import java.util.Objects;

 

public class Employee {

          

           int id;

 

           String name;

 

           int age;

 

           int salary; // per annum

 

           String department;

 

           String city;

          

          

          

           public Employee(int id, String name, int age, int salary, String department, String city)throws IllegalArgumentExceptionDemo {

                      super();

                     

                      if(id<0)

                      {

                                 throw new IllegalArgumentExceptionDemo("exception occured, id cannot less than zero");

                      }

                      this.id = id;

                      if(name==null) {

                                 throw new IllegalArgumentExceptionDemo("exception occured, name cannot be null");

 

                      }

                      this.name = name;

                      if(age<=0)

                      {

                                 throw new IllegalArgumentExceptionDemo("exception occured, age cannot be zero");

 

                      }

                      this.age = age;

                      if(salary<0)

                      {

                                 throw new IllegalArgumentExceptionDemo("exception occured, salary cannot be zero");

 

                      }

                      this.salary = salary;

                      if( department == null) {

                                 throw new IllegalArgumentExceptionDemo("exception occured, department cannot be null");

 

                      }

                      this.department = department;

                      if(city==null)

                      {

                                 throw new IllegalArgumentExceptionDemo("exception occured, city cannot be null");

 

                      }

                      this.city = city;

                     

           }

          

          

          

          

          

          

          

 

           public int getId() {

                      return id;

           }

 

           public void setId(int id) {

                      this.id = id;

           }

 

           public String getName() {

                      return name;

           }

 

           public void setName(String name) {

                      this.name = name;

           }

 

           public int getAge() {

                      return age;

           }

 

           public void setAge(int age) {

                      this.age = age;

           }

 

           public int getSalary() {

                      return salary;

           }

 

           public void setSalary(int salary) {

                      this.salary = salary;

           }

 

           public String getDepartment() {

                      return department;

           }

 

           public void setDepartment(String department) {

                      this.department = department;

           }

 

           public String getCity() {

                      return city;

           }

 

           public void setCity(String city) {

                      this.city = city;

           }

          

 

           @Override

           public String toString() {

                      return " [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", department="

                                            + department + ", city=" + city + "]";

           }

 

 

 

 

           @Override

           public int hashCode() {

                      return Objects.hash(age, city, department, id, name, salary);

           }

 

 

 

 

 

           @Override

           public boolean equals(Object obj) {

                      if (this == obj)

                                 return true;

                      if (obj == null)

                                 return false;

                      if (getClass() != obj.getClass())

                                 return false;

                      Employee other = (Employee) obj;

                      return age == other.age && Objects.equals(city, other.city) && Objects.equals(department, other.department)

                                            && id == other.id && Objects.equals(name, other.name) && salary == other.salary;

           }

 

          

          

          

 

}

