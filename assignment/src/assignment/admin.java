package com.hcl.gl.assessment;


public class AdminDep extends SuperDep {


//declare method departmentName of return type string
public String depName() {
return "ADMIN DEPARTMENT";
}

//declare method getTodaysWork of return type string
public String getTodaysWork() {
return "COMPLETE YOUR DOCUMENTS SUBMISSION";
}

//declare method getWorkDeadline of return type string
public String getWorkDeadline() {
return "COMPLETE BY END OF THE DAY ";
}

}
//Hrdep Class
package com.hcl.gl.assessment;

public class HrDep extends SuperDep {
public String depName() {
return "HR DEPARTMENT ";
}


public String getTodaysWork() {
return "FILL UR WORK SHEET AND MARK UR ATTENDANCE ";
}

public String doactivity(){
return "TEAM LAUNCH";
}
public String getWorkDeadline() {
return " COMPLETE BY END OF DAY";

}


}
//SuperDep Class
package com.hcl.gl.assessment;

public class SuperDep {
public String depName() {
return "SUPERDEPARTMENT ";
}

//declare method getTodaysWork of return type string
public String getTodaysWork() {
return "NO WORK AS OF NOW ";
}

//declare method getWorkDeadline of return type string
public String getWorkDeadline() {
return " NIL ";
}

//declare method isTodayAHoliday of type string
public String isTodayAHoliday() {
return "TODAY IS NOT HOLIDAY";
}

}
//TechDep Class
package com.hcl.gl.assessment;

public class TechDep extends SuperDep {
public String depName() {
return "TECH DEPARTMENT";
}
public String getTodaysWork() {
return "COMPLETE CODING OF MODULE ONE";
}
public String getTechStackInformation() {
return "CORE JAVA";
}
public String getWorkDeadline() {
return " COMPLETE IT BY END OF DAY";
}

}
//Main Class
package com.hcl.gl.assessment;
import java.util.Scanner;
public class Main{
public static void main(String[] args) {
while (true) {
Scanner scr = new Scanner(System.in);
System.out.println("enter 1 for super department"+
                    "enter 2 for admin department"+
         "enter 3 for hr department"+
                    "enter 4 for tech department"+
         "enter 5 for exit");
 int a=scr.nextInt();
 switch(a) {
 case 1:
  SuperDep pri = new SuperDep();
  System.out.println(pri.depName());
  System.out.println(pri.getTodaysWork());
  System.out.println(pri.getWorkDeadline());
  System.out.println(pri.isTodayAHoliday());
  break;
 case 2 :
  AdminDep pri1 = new AdminDep();
  System.out.println(pri1.depName());
  System.out.println(pri1.getTodaysWork());
  System.out.println(pri1.getWorkDeadline());
  break;
 case 3 :
  HrDep pri2 = new HrDep();
  System.out.println(pri2.depName());
  System.out.println(pri2.getTodaysWork());
  System.out.println(pri2.doactivity());
  System.out.println(pri2.getWorkDeadline());
  break;
 case 4:
  TechDep pri3 = new TechDep();
  System.out.println(pri3.depName());
  System.out.println(pri3.getTodaysWork());
  System.out.println(pri3.getTechStackInformation());
  System.out.println(pri3.getWorkDeadline());
  break;
 case 5:
  System.exit(1);
 default:
  System.out.println("Enter invalid option");
 
 }
 }
}

}