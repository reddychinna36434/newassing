create database travel_hcl;
use travel_hcl;
create table PASSENGER_hcl
 (Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
  );
select * from PASSANGER_hcl;

create table PRICE1
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );
select * from PRICE1;
 
insert into passenger_hcl values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger_hcl values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger_hcl values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger_hcl values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger_hcl values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger_hcl values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger_hcl values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger_hcl values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger_hcl values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');
        
insert into price1 values('Sleeper',350,770);
insert into price1 values('Sleeper',500,1100);
insert into price1 values('Sleeper',600,1320);
insert into price1 values('Sleeper',700,1540);
insert into price1 values('Sleeper',1000,2200);
insert into price1 values('Sleeper',1200,2640);
insert into price1 values('Sleeper',350,434);
insert into price1 values('Sitting',500,620);
insert into price1 values('Sitting',500,620);
insert into price1 values('Sitting',600,744);
insert into price1 values('Sitting',700,868);
insert into price1 values('Sitting',1000,1240);
insert into price1 values('Sitting',1200,1488);
insert into price1 values('Sitting',1500,1860);
/* 1.	How many female and how many male passengers travelled for a minimum distance of 600 KM s?*/

select count(*) gender from passenger_hcl where distance<=600 group by gender like '%F'
 order by gender like '%M';
 /* 2.	Find the minimum ticket price for Sleeper Bus.*/
 select min(Price) from Price1 where Bus_Type='Sleeper';
/* 3.	Select passenger names whose names start with character 'S'. */
SELECT * FROM passenger_hcl WHERE Passenger_name LIKE 'S%';

/* 4.	Calculate price charged for each passenger displaying Passenger name, Boarding City, Destination City, Bus_Type, Price in the output.*/

select  Passenger_hcl.Passenger_name,Passenger_hcl.Boarding_City,Passenger_hcl.Destination_City,Passenger_hcl.Bus_Type,Price1.Price 
from passenger_hcl ,price1 
where Passenger_hcl.Distance=Price1.Distance and Passenger_hcl.Bus_Type=Price1.Bus_Type group by Passenger_hcl.Passenger_name;

/*5.	What is the passenger name and his/her ticket price who travelled in Sitting bus for a distance of 1000 KMs.*/

select Passenger_hcl.Passenger_name,Price1.Price from passenger_hcl ,price1  
where Passenger_hcl.Bus_Type='Sitting' and passenger_hcl.Distance=1000;

/*6.	What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji?*/
 
select Price from price1,passenger_hcl 
where  passenger_hcl.Distance=price1.Distance and passenger_hcl.Passenger_name="Pallavi"; 

/* 7.	List the distances from the "Passenger" table which are unique (non-repeated distances) in descending order.*/

select  distinct distance from Passenger_hcl
 order by distance desc;
 
 /* 8.Display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers without using user variables.*/
select  Passenger_name,(Distance/c.h)*100 as percentage 
 from passenger_hcl cross join (select sum(Distance) as h from passenger_hcl)c;
 
/* 9.	Create a view to see all passengers who travelled in AC Bus.*/
create view pass as select * from passenger_hcl
where Category='Ac';
/* 10.	Create a stored procedure to find total passengers traveled using Sleeper buses.*/
delimiter //
create procedure storedpassenger() begin
select count(*) from passenger_hcl where Bus_type='Sleeper';
end //

/* 11.	Display 5 records at one time */
SELECT * FROM passenger_hcl ORDER BY passenger_name  LIMIT 5;
